package suelos;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class ConexionSuelos {

    //1. comonecrtarse base de datos 
    Connection miConexion;
    PreparedStatement clausula;

    boolean conectar() {
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            //Creo un objeto de la clase conexion
            //No se recomienda usar el usuario de root como 
            //Obtiene conexion tienen un objeto y se conecta a la base de
            miConexion = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/suelos caracteristicas?autoReconnect=true&useSSL=false", "root", "mysql2019");
            System.out.print("me conecte a esta porqueria insegura");
        } catch (Exception e) {
            System.out.print(e);
        }
        return true;
    }

    boolean escribir() {

        String Textura = "Arenosa";
        float Temperatura = (float) 35.85;
        String PH = "Base";
        String Color = "Negro";

        try {
            String insertar = "INSERT INTO `suelos caracteristicas`.`caracteristicas`(`Textura`,`Temperatura`,`PH`,`Color`)VALUES(?,?,?,?)";
            
            clausula = miConexion.prepareStatement(insertar);
            clausula.setString(1,Textura);
            clausula.setDouble(2, Temperatura);
            clausula.setString(3, PH);
            clausula.setString(4, Color);

            clausula.executeUpdate();

        } catch (Exception e) {
            System.out.print(e);
        }

        return true;
    }
}
